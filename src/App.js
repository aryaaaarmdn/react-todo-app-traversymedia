import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import './App.css';

// import axios from 'axios' // kedua
import { v4 as uuidv4 } from 'uuid'; // pertama

import Todos from './components/Todos'
import Header from './components/Layout/Header'
import AddTodo from './components/AddTodo'
import About from './components/pages/About'

class App extends React.Component {

  //  Pertama
  state = {
    todos: [
      {
        id: uuidv4(),
        title: 'Take out the trash',
        completed: false
      },
      {
        id: uuidv4(),
        title: 'Take a nap',
        completed: true
      },
      {
        id: uuidv4(),
        title: 'Playing',
        completed: false
      },
    ]
  }


  //kedua
  // state = {
  //   todos: []
  // }

  // componentDidMount() {
  //   axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
  //     .then(response => this.setState({ todos: response.data }))
  // }

  // Toggle Complete
  markComplete = (id) => {
    this.setState({
      todos: this.state.todos.map(todo => {
        if (todo.id === id) todo.completed = !todo.completed
        return todo;
      })
    })
  }

  // delete todo
  delTodo = (id) => {

    // pertama
    this.setState({
      todos: [...this.state.todos.filter(todo => todo.id !== id)]
    })

    //kedua
    // axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    //   .then(response => this.setState({
    //     todos: [...this.state.todos.filter(todo => todo.id !== id)]
    //   }))
  }

  // add todo
  addTodo = (title) => {
    // pertama
    const newTodo = {
      id: uuidv4(),
      title,
      completed: false
    }
    this.setState({ todos: [...this.state.todos, newTodo] })

    // kedua
    // axios.post('https://jsonplaceholder.typicode.com/todos', {
    //   title: title,
    //   completed: false
    // })
    //   .then(response => this.setState({ todos: [...this.state.todos, response.data] }))

  }

  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
            <Header />
            <Route exact path="/" render={props => (
              <React.Fragment>
                <AddTodo addTodo={this.addTodo} />
                <Todos todos={this.state.todos} markComplete={this.markComplete} delTodo={this.delTodo} />
              </React.Fragment>
            )} />
            <Route path="/about" component={About} />
          </div>
        </div>
      </Router>
    )
  }

}

export default App;
