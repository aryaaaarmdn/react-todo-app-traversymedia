import React from 'react'

export default function About() {
    return (
        <React.Fragment>
            <h1>About</h1>
            <p>This is to-do list app crash course from Traversy Media</p>
        </React.Fragment>
    )
}
