import React from 'react'
import { Link } from 'react-router-dom'

const linkStyle = {
    color: '#fff',
    textDecoration: 'none'
}

function Header() {
    return (
        <header style={headerStyle}>
            <h1>To-do List</h1>
            <Link to="/" style={linkStyle}>Home</Link> | <Link to="/about" style={linkStyle}>About</Link>
        </header>
    )
}

const headerStyle = {
    backgroundColor: '#333',
    color: '#fff',
    textAlign: 'center',
    padding: '10px'
}

export default Header